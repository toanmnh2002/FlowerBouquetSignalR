﻿using BusinessObjects.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcess
{
    public class CategoryDAO
    {
        private readonly FuflowerBouquetManagementContext _dbContext;

        public CategoryDAO()
        {
            _dbContext = new FuflowerBouquetManagementContext();
        }
        public async Task<IEnumerable<Category>> GetCategoryList()
        {
            IEnumerable<Category> categories = null;
            try
            {
                categories = await _dbContext.Categories.ToListAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return categories;
        }

        public async Task<Category> GetCategoryById(int categoryId)
        {
            Category category = null;
            try
            {
                category = await _dbContext.Categories.FirstOrDefaultAsync(cate => cate.CategoryId == categoryId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return category;
        }

        public async Task<Category> GetCategoryByName(string categoryName)
        {
            Category category = null;
            try
            {
                category = await _dbContext.Categories.FirstOrDefaultAsync(cate => cate.CategoryName.Equals(categoryName.Trim()));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return category;
        }

        public async Task AddCategory(string categoryName)
        {
            try
            {
                if (GetCategoryByName(categoryName) is not null)
                {
                    throw new Exception("Category name is existed!!!");
                }
                else
                {
                    await _dbContext.Categories.AddAsync(new Category
                    {
                        CategoryName = categoryName
                    });
                    await _dbContext.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}

