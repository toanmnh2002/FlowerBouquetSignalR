﻿using BusinessObjects.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcess
{
    public class SupplierDAO
    {
        private readonly FuflowerBouquetManagementContext _dbContext;

        public SupplierDAO()
        {
            _dbContext = new FuflowerBouquetManagementContext();
        }
        public async Task<IEnumerable<Supplier>> GetSupplierList()
        {
            IEnumerable<Supplier> suppliers = null;
            try
            {
                suppliers = await _dbContext.Suppliers.ToListAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return suppliers;
        }

        public async Task<Supplier> GetCategoryById(int supplierId)
        {
            Supplier supplier = null;
            try
            {
                supplier = await _dbContext.Suppliers.FirstOrDefaultAsync(x => x.SupplierId == supplierId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return supplier;
        }

        public async Task<Supplier> GetSupplierByName(string supplierName)
        {
            Supplier supplier = null;
            try
            {
                supplier = await _dbContext.Suppliers.FirstOrDefaultAsync(cate => cate.SupplierName.Equals(supplierName.Trim()));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return supplier;
        }

        public async Task AddSupplier(string supplierName)
        {
            try
            {
                if (GetSupplierByName(supplierName) is not null)
                {
                    throw new Exception("Supplier name is existed!!!");
                }
                else
                {
                    await _dbContext.Suppliers.AddAsync(new Supplier
                    {
                        SupplierName = supplierName
                    });
                    await _dbContext.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}

