﻿using BusinessObjects.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace DataAcess
{
    public class OrderDAO
    {
        private readonly FuflowerBouquetManagementContext _dbContext;

        public OrderDAO()
        {
            _dbContext = new FuflowerBouquetManagementContext();
        }
        public async Task<IEnumerable<Order>> GetByCustomerId(int customerId)
        {

            IEnumerable<Order> orders = null;

            try
            {
                if (customerId > 0)
                {
                    orders = await _dbContext.Orders.Where(or => or.CustomerId == customerId)
                        .Include(or => or.Customer)
                        .Include(or => or.OrderDetails).ToListAsync();
                }
                else
                {
                    orders = await _dbContext.Orders.Include(or => or.Customer)
                        .Include(or => or.OrderDetails).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return orders;
        }
        public async Task<Order> GetByOrderId(int id)
        {
            var result = await _dbContext.Orders.Where(x=>x.OrderId==id).AsQueryable().Include(x=>x.CustomerId).FirstAsync();
            return result;
        }

        public async Task<IEnumerable<Order>> GetAll()
        {
            var result = await _dbContext.Orders.Include(c => c.Customer).Include(x=>x.OrderDetails).ToListAsync();
            return result;
        }
        public async Task<IEnumerable<Order>> GetOrdersByDateRange(int CustomerId, DateTime startDate, DateTime endDate)
        {
            IQueryable<Order> query = _dbContext.Orders.Include(x => x.Customer).Include(x => x.OrderDetails);
            //IEnumerable<Order> orders = null;
            try
            {
                if (CustomerId > 0)
                {
                    query = query.Where(x => x.CustomerId == CustomerId);
                }
                var orders = await query
               .Where(x => x.OrderDate >= startDate && x.OrderDate <= endDate)
               .ToListAsync();
                return orders;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<Order> Add(Order order)
        {
            Order newOrder = null;
            try
            {
                await _dbContext.AddAsync(order);
                await _dbContext.SaveChangesAsync();
                newOrder = order;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return newOrder;
        }

        public async Task Update(Order order)
        {
            if (order is null)
            {
                throw new Exception("Order is undefined!");
            }
            try
            {
                var _order = await GetByOrderId(order.OrderId);
                if (_order is not null)
                {
                    _dbContext.Orders.Update(order);
                    await _dbContext.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task DeleteByID(int id)
        {
            try
            {
                var _order = await GetByOrderId(id);
                if (_order is not null)
                {
                    _dbContext.Orders.Remove(_order);
                    await _dbContext.SaveChangesAsync();
                }
                else
                {
                    throw new Exception("Order does not exist!!");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task DeleteByCustomer(int customerId)
        {
            try
            {
                var _order = await GetByCustomerId(customerId);
                if (_order is not null)
                {
                    _dbContext.Orders.RemoveRange(_order);
                    await _dbContext.SaveChangesAsync();
                }
                else
                {
                    throw new Exception("Order does not exist!!");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}