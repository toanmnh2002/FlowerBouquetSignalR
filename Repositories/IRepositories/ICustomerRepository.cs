﻿using BusinessObjects.Entities;
using DataAcess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.IRepositories
{
    public interface ICustomerRepository
    {
        Task<Customer> GetByEmail(string email);
        Task<Customer> GetCustomerByID(int customerID);
        Task<IEnumerable<Customer>> GetAll();
        Task<IEnumerable<Customer>> GetAllCustomer();
        Task AddCustomer(Customer customer);
        Task UpdateCustomer(Customer customer);
        Task DeleteCustomer(int customerID);
        Task<int> GetNextMemberId();
        Task<IEnumerable<Customer>> SearchCustomer(string name);
        Task<IEnumerable<Customer>> SearchCustomerByCountry(string country, IEnumerable<Customer> searchList);
        Task<IEnumerable<Customer>> SearchCustomerByCity(string country, string city, IEnumerable<Customer> searchList);
        Task<Customer> Login(string email, string password);
        Task<bool> IsLogin(string email);
        Task<bool> IsCustomer(string email);
        Task<bool> IsAdmin(string email);
    }
}
