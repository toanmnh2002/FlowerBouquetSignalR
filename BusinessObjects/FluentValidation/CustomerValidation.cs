﻿using BusinessObjects.Entities;
using FluentValidation;

namespace BusinessObjects.FluentValidation
{
    public class CustomerValidation : AbstractValidator<Customer>
    {
        public CustomerValidation()
        {
            RuleFor(x => x.CustomerId)
               .NotEmpty().WithMessage("CustomerId ID is required.")
               .GreaterThan(0).WithMessage("CustomerId ID must be greater than 0.");

            RuleFor(x => x.Email)
                .NotEmpty()
                .WithMessage("Email is required.")
                .EmailAddress()
                .WithMessage("Email is not correct form.")
                .MaximumLength(100)
                .WithMessage("Email at most 100 characters ");

            RuleFor(x => x.CustomerName)
                .NotEmpty()
                .WithMessage("Customer name is required.")
                .MaximumLength(180)
                .WithMessage("Customer name at most 180 characters ")
                .MustAsync(NotContainsNumberAsync)
                .WithMessage("CustomerName cannot contain numbers.");

            RuleFor(x => x.City)
                .NotEmpty()
                .WithMessage("City is required.")
                .MaximumLength(15)
                .WithMessage("City at most 15 characters ")
                .MustAsync(NotContainsNumberAsync)
                .WithMessage("City cannot contain numbers."); ;

            RuleFor(x => x.Country)
                .NotEmpty()
                .WithMessage("Country is required.")
                .MaximumLength(15)
                .WithMessage("Country at most 100 characters ")
                .MustAsync(NotContainsNumberAsync)
                .WithMessage("Country cannot contain numbers.");

            RuleFor(x => x.Password)
                .NotEmpty()
                .WithMessage("Password is required.")
                .Matches(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,30}$")
                .WithMessage("Password must be at least 8 characters, at most 30 characters long and contain at least one uppercase letter, one lowercase letter, one digit, and one special character.");

            RuleFor(x => x.Birthday)
                .NotEmpty()
                .WithMessage("Birthday is required.")
                .MustAsync(BeValidBirthdayAsync)
                .WithMessage("This the day in the future:)).");
        }
        private async Task<bool> BeValidBirthdayAsync(DateTime? birthday, CancellationToken cancellationToken)
        {
            if (birthday.HasValue)
            {
                DateTime currentDate = DateTime.Now;
                return await Task.FromResult(birthday.Value < currentDate);
            }
            return false;
        }
        private async Task<bool> NotContainsNumberAsync(string value, CancellationToken cancellationToken)
        {
            if (!string.IsNullOrEmpty(value) && !value.Any(char.IsDigit))
            {
                return true;
            }
            return false;
        }
    }
}