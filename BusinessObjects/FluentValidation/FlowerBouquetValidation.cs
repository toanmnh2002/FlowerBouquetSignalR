﻿using BusinessObjects.Entities;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects.FluentValidation
{
    public class FlowerBouquetValidation : AbstractValidator<FlowerBouquet>
    {
        public FlowerBouquetValidation()
        {
            RuleFor(x => x.FlowerBouquetId)
                 .NotEmpty().WithMessage("FlowerBouquetId ID is required.")
                 .GreaterThan(0).WithMessage("FlowerBouquetId ID must be greater than 0.");

            RuleFor(x => x.CategoryId)
                .NotEmpty().WithMessage("Please select Category Name.");
            RuleFor(x => x.SupplierId)
                .NotEmpty().WithMessage("Please select Category Name.");

            RuleFor(x => x.FlowerBouquetName)
                .NotEmpty().WithMessage("Flower bouquet name is required.")
                .MaximumLength(40).WithMessage("Flower bouquet name cannot exceed 40 characters.")
                .MustAsync(NotContainsNumberAsync).WithMessage("FlowerBouquetName cannot contain numbers.");

            RuleFor(x => x.Description)
                .NotEmpty().WithMessage("Description is required.")
                .MaximumLength(220).WithMessage("Description cannot exceed 220 characters.");

            RuleFor(x => x.UnitPrice)
                .NotEmpty().WithMessage("Unit price is required.")
                .GreaterThan(0).WithMessage("Unit price must be greater than 0.")
                .MustAsync(NotContainsDigitAsync).WithMessage("Unit Price cannot contain numbers.");

            RuleFor(x => x.UnitsInStock)
                .NotEmpty().WithMessage("Units in stock is required.")
                .GreaterThan(0).WithMessage("UnitsInStock must be greater than 0.");
        }

        private async Task<bool> NotContainsDigitAsync(decimal value, CancellationToken arg2)
        {
            string valueString = value.ToString();
            if (!string.IsNullOrEmpty(valueString) && !valueString.Any(char.IsDigit))
            {
                return false;
            }
            return true;
        }

        private async Task<bool> NotContainsNumberAsync(string value, CancellationToken cancellationToken)
        {
            if (!string.IsNullOrEmpty(value) && !value.Any(char.IsDigit))
            {
                return true;
            }
            return false;
        }
    }
}


