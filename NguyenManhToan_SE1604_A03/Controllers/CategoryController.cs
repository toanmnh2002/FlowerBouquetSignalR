﻿using Microsoft.AspNetCore.Mvc;
using Repositories.IRepositories;
using Repositories.Repositories;

namespace NguyenManhToan_SE1604_A02.Controllers
{
    public class CategoryController : BaseController
    {
        public CategoryController(ICustomerRepository customerRepository, IOrderRepository orderRepository, IOrderDetailRepository orderDetailRepository, IFlowerBouquetRepository flowerBouquetRepository, ICategoryRepository categoryRepository, ISupplierRepository supplierRepository) : base(customerRepository, orderRepository, orderDetailRepository, flowerBouquetRepository, categoryRepository, supplierRepository)
        {
        }

        public async Task<IActionResult> Create(string categoryName)
        {
            try
            {
                if (!string.IsNullOrEmpty(categoryName))
                {
                    await _categoryRepository.AddCategory(categoryName);
                    return Json("Create Category successfully!!");
                }
                else
                {
                    throw new Exception("The Category Name is empty!!");
                }
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }

        }
    }
}
