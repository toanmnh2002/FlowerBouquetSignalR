﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BusinessObjects.Entities;
using Repositories.IRepositories;
using FluentValidation;
using FluentValidation.Results;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.SignalR;

namespace NguyenManhToan_SE1604_A02.Controllers
{
    public class CustomerController : BaseController
    {
        private readonly IValidator<Customer> _validation;
        private readonly IHubContext<SignalrServer> _signalHub;

        public CustomerController(ICustomerRepository customerRepository, IOrderRepository orderRepository, IOrderDetailRepository orderDetailRepository, IFlowerBouquetRepository flowerBouquetRepository, ICategoryRepository categoryRepository, ISupplierRepository supplierRepository, IValidator<Customer> validation, IHubContext<SignalrServer> signalHub) : base(customerRepository, orderRepository, orderDetailRepository, flowerBouquetRepository, categoryRepository, supplierRepository)
        {
            _validation = validation;
            _signalHub = signalHub;
        }


        // GET: Customer
        public async Task<IActionResult> Index()
        {
            string userLogin = HttpContext.Session.GetString("USERLOGIN");
            if (!(await _customerRepository.IsAdmin(userLogin)))
            {
                return RedirectToAction("Index", "User");
            }
            var customers = await _customerRepository.GetAllCustomer();
            return customers is not null ?
                        View(customers.ToList()) :
                        Problem("Entity set 'FuflowerBouquetManagementContext.Customers'  is null.");
        }
        //SignalR
        [HttpGet]
        public async Task<IActionResult> GetCustomers()
        {
            var result = await _customerRepository.GetAllCustomer();
            return Ok(result);
        }
        // GET: Customer/Details/5
        public async Task<IActionResult> Details(int id)
        {
            try
            {
                string userLogin = HttpContext.Session.GetString("USERLOGIN");
                if (!(await _customerRepository.IsAdmin(userLogin)))
                {
                    return RedirectToAction("Index", "User");
                }
                if (await _customerRepository.GetCustomerByID(id) is null)
                {
                    throw new Exception("Customer ID is not found!!!");
                }
                var customer = await _customerRepository.GetCustomerByID(id);
                if (customer is null)
                {
                    throw new Exception("Customer ID is not found!!!");
                }
                return View(customer);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View();
            }
        }
        // GET: Customer/Details/5
        public async Task<IActionResult> ViewProfile()
        {
            try
            {
                int CustomerId = int.Parse(HttpContext.Session.GetString("CustomerId"));
                string userLogin = HttpContext.Session.GetString("USERLOGIN");
                if (await _customerRepository.IsCustomer(userLogin) is false)
                {
                    return RedirectToAction("Index", "User");
                }
                if (await _customerRepository.GetCustomerByID(CustomerId) is null)
                {
                    throw new Exception("Customer ID is not found!!!");
                }
                var customer = await _customerRepository.GetCustomerByID(CustomerId);
                if (customer is null)
                {
                    throw new Exception("Customer ID is not found!!!");
                }
                return View(customer);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View();
            }
        }
        public async Task<IActionResult> Register()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register([Bind("CustomerId,Email,CustomerName,City,Country,Password,Birthday")] Customer customer)
        {
            try
            {
                ValidationResult rs = await _validation.ValidateAsync(customer);
                if (rs.IsValid)
                {
                    await _customerRepository.AddCustomer(customer);
                    await _signalHub.Clients.All.SendAsync("LoadCustomers");
                    return RedirectToAction(nameof(Index));
                }
                rs.AddToModelState(this.ModelState);
                return View("Register", customer);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View("Register");
            }
            //return null;
        }
        // GET: Customer/Create
        public async Task<IActionResult> Create()
        {
            string userLogin = HttpContext.Session.GetString("USERLOGIN");
            if (!(await _customerRepository.IsAdmin(userLogin)))
            {
                return RedirectToAction("Index", "User");
            }
            return View();
        }

        // POST: Customer/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CustomerId,Email,CustomerName,City,Country,Password,Birthday")] Customer customer)
        {
            try
            {
                //if (ModelState.IsValid)
                //{
                ValidationResult rs = await _validation.ValidateAsync(customer);
                if (rs.IsValid)
                {
                    await _customerRepository.AddCustomer(customer);
                    await _signalHub.Clients.All.SendAsync("LoadCustomers");
                    return RedirectToAction(nameof(Index));
                }
                rs.AddToModelState(this.ModelState);
                return View("Create", customer);
            }
            //}
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View("Create");
            }
            //return null;
        }

        // GET: Customer/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            try
            {
                string userLogin = HttpContext.Session.GetString("USERLOGIN");
                if (!(await _customerRepository.IsLogin(userLogin)))
                {
                    return RedirectToAction("Index", "User");
                }
                if (await _customerRepository.GetCustomerByID(id) is null)
                {
                    throw new Exception("Customer ID is not found!!!");
                }
                var customer = await _customerRepository.GetCustomerByID(id);
                if (customer is null)
                {
                    throw new Exception("Customer ID is not found!!!");
                }

                return View(customer);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View();
            }
        }

        // POST: Customer/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int CustomerId, [Bind("CustomerId,Email,CustomerName,City,Country,Password,Birthday")] Customer customer)
        {
            try
            {
                ValidationResult rs = await _validation.ValidateAsync(customer);
                if (rs.IsValid)
                {
                    await _customerRepository.UpdateCustomer(customer);
                    await _signalHub.Clients.All.SendAsync("LoadCustomers");
                    ViewBag.Success = "Update Successfully!";
                    return View("Edit", customer);
                }
                rs.AddToModelState(this.ModelState);
                return View("Edit", customer);
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
                return View("Edit");
            }
        }

        // GET: Customer/Delete/5
        public async Task<IActionResult> Delete(int id)
        {
            string userLogin = HttpContext.Session.GetString("USERLOGIN");
            if (!(await _customerRepository.IsAdmin(userLogin)))
            {
                return RedirectToAction("Index", "User");
            }
            if (await _customerRepository.GetCustomerByID(id) is null)
            {
                return NotFound();
            }

            var customer = await _customerRepository.GetCustomerByID(id);
            if (customer is null)
            {
                return NotFound();
            }

            return View(customer);
        }

        // POST: Customer/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int CustomerId)
        {
            if (await _customerRepository.GetCustomerByID(CustomerId) is null)
            {
                return Problem("Entity set 'FuflowerBouquetManagementContext.Customers'  is null.");
            }
            var customer = await _customerRepository.GetCustomerByID(CustomerId);
            if (customer is not null)
            {
                await _customerRepository.DeleteCustomer(customer.CustomerId);
                await _signalHub.Clients.All.SendAsync("LoadCustomers");
            }
            return RedirectToAction(nameof(Index));
        }
        public async Task<IActionResult> Search(string search)
        {
            string userLogin = HttpContext.Session.GetString("USERLOGIN");
            if (!(await _customerRepository.IsAdmin(userLogin)))
            {
                return RedirectToAction("Index", "User");
            }
            var searchList = await _customerRepository.GetAllCustomer();
            if (!string.IsNullOrEmpty(search))
            {
                searchList = await _customerRepository.SearchCustomer(search);
            }
            return View(searchList);
        }
    }
}
