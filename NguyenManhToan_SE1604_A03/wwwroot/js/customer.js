﻿$(() => {
    var connection = new signalR.HubConnectionBuilder().withUrl("/signalrServer").build();

    connection.on("LoadCustomers", function () {
        LoadProdData();
    });

    connection.start()
        .then(function () {
            LoadProdData();
        })
        .catch(function (error) {
            console.log("SignalR connection error: " + error);
        });

    function LoadProdData() {
        $.ajax({
            url: '/Customer/GetCustomers',
            method: 'GET',
            success: function (result) {
                var tr = '';
                $.each(result, function (k, v) {
                    tr += `<tr>
                        <td>${v.CustomerId}</td>
                        <td>${v.Email}</td>
                        <td>${v.CustomerName}</td>
                        <td>${v.City}</td>
                        <td>${v.UnitPrice}</td>
                        <td>${v.Country}</td>
                        <td>${v.Password}</td>
                        <td>${v.Birthday}</td>
                        <td>
                            <a href='../Customer/Edit?id=${v.CustomerId}'>Edit</a> |
                            <a href='../Customer/Details?id=${v.CustomerId}'>Details</a> |
                            <a href='../Customer/Delete?id=${v.CustomerId}'>Delete</a>
                        </td>
                    </tr>`;
                });
                $("#tableBody1").html(tr);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }
});
