﻿$(() => {
    var connection = new signalR.HubConnectionBuilder().withUrl("/signalrServer").build();

    connection.on("LoadFlowerBouquets", function () {
        LoadProdData();
    });

    connection.start()
        .then(function () {
            LoadProdData();
        })
        .catch(function (error) {
            console.log("SignalR connection error: " + error);
        });

    function LoadProdData() {
        $.ajax({
            url: '/FlowerBouquet/GetFlowerBouquets',
            method: 'GET',
            success: function (result) {
                var tr = '';
                $.each(result, function (k, v) {
                    tr += `<tr>
                        <td>${v.FlowerBouquetId}</td>
                        <td>${v.FlowerBouquetName}</td>
                        <td>${v.Description}</td>
                        <td>${v.UnitPrice}</td>
                        <td>${v.UnitsInStock}</td>
                        <td>${v.FlowerBouquetStatus}</td>
                        <td>${v.Category.CategoryName}</td>
                        <td>${v.Supplier.SupplierName}</td>
                        <td>
                            <a href='../FlowerBouquet/Edit?id=${v.FlowerBouquetId}'>Edit</a> |
                            <a href='../FlowerBouquet/Details?id=${v.FlowerBouquetId}'>Details</a> |
                            <a href='../FlowerBouquet/Delete?id=${v.FlowerBouquetId}'>Delete</a>
                        </td>
                    </tr>`;
                });
                $("#tableBody").html(tr);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }
});
