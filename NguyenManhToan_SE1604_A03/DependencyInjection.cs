﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore;
using System.Configuration;
using System;
using BusinessObjects.Entities;
using Repositories.IRepositories;
using Repositories.Repositories;
using DataAcess;
using FluentValidation;
using BusinessObjects.FluentValidation;
using FluentValidation.AspNetCore;
using System.Text.Json.Serialization;

namespace NguyenManhToan_SE1604_A02
{
    public static class DependencyInjection
    {
        public static IServiceCollection AppService(this IServiceCollection services, IConfiguration configuration)
        {
            //Connect to DB
            //services.AddDbContext<FuflowerBouquetManagementContext>(options =>
            //            options.UseSqlServer(configuration.GetConnectionString("DatabaseConnection")));
            //services.AddScoped<IUnitOfWork, UnitOfWork>();
            //Repository
            services.AddScoped<ICustomerRepository, CustomerRepository>();
            services.AddScoped<IFlowerBouquetRepository, FlowerBouquetRepository>();
            services.AddScoped<IOrderRepository, OrderRepository>();
            services.AddScoped<IOrderDetailRepository, OrderDetailRepository>();
            services.AddScoped<ICategoryRepository, CategoryRepository>();
            services.AddScoped<ISupplierRepository, SupplierRepository>();
            //Fluent Validation
            services.AddScoped<IValidator<Customer>, CustomerValidation>();
            services.AddScoped<IValidator<FlowerBouquet>, FlowerBouquetValidation>();
            services.AddScoped<IValidator<Order>, OrderValidation>();
            services.AddControllersWithViews();
            services.AddRazorPages();
            // Using session
            services.AddDistributedMemoryCache(); // Store Cache in Memory
            services.AddSession();
            //Config signalR
            services.AddSignalR();
            services.AddControllers().AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.PropertyNamingPolicy = null;
            }).AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
            });

            return services;
        }
    }
}
